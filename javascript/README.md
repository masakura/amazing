# JavaScript
コンストラクター関数は `return` が書ける。

```javascript
class Class1 {
    constructor() {
        return "hello";
    }
}
```

`retrun "hello"` は無視される。

```javascript
class Class1 {
    constructor() {
        return { name: "hello" };
    }
}
```

`new Class1()` は `{ name: "hello" }` になる。

ES5 以前でも同じでした。