class Age {
    #value;

    /**
     * @param {number|Age} value
     */
    constructor(value) {
        if (value instanceof Age) return value;

        this.#value = value;
    }
}

const age1 = new Age(20);
const age2 = new Age(age1);
console.log(age1 === age2);
