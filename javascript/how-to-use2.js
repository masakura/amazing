'use strict';

class ImmutableValue {
    constructor(value) {
        this.value = value;
        return Object.freeze(this);
    }
}

const value = new ImmutableValue(1);
value.value = 2;