package main

import "fmt"

func main() {
    fmt.Println(func1())
}

type Vector struct {
    x int
    y int
}

func func1() Vector {
    a := Vector{x: 1, y: 2}
    return a
}
