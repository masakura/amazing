# Go でびびったこと

```Go
func func1() Vector {
    a := Vector{x: 1, y: 2}
    return a
}
```
上のコードは値渡し。変数 `a` はスタックに確保される。C 言語でも同様。

```Go
func func1() *Vector {
    a := Vector{x: 1, y: 2}
    return &a
}
```

C 言語では NG な、スタック変数のポインター渡しがなぜか使える。このケースでは `a` はヒープに確保されるとのこと。