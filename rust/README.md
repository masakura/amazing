# Rust
Rust は貸し借りがどうたらが有名だけど...

enum 型が結構すごい。

```Rust
enum Node {
    Null,
    String(String),
    Array(Vec<Node>),
}
```

上のは、`Null` / `String` / `Array` と三つあるけど、`String(String)` ってなってて、`String` 型を引数に持つことができる。

```Rust
fn to_json(node: &Node) -> String {
    match node {
        Node::Null => "null".to_string(),
        Node::String(s) => format!("\"{}\"", s),
        Node::Array(nodes) => format!("[{}]", nodes.iter().map(|n| to_json(n)).collect::<Vec<_>>().join(",").to_string()),
    }
}
```

こんな感じで、JSON 出力とかが静的に書ける。

すごいのが、こんな感じで要素を増やすと...

```Rust
enum Node {
    Null,
    Number(i32), // 面倒だから整数のみ...
    String(String),
    Array(Vec<Node>),
}
```

`to_json()` 関数はコンパイルエラーになる。