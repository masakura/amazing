fn main() {
    println!("{}", to_json(&Node::Null));
    println!("{}", to_json(&Node::String("hello".to_string())));
    println!("{}", to_json(&Node::Array(vec![
        Node::Null,
        Node::String("a".to_string()),
    ])));
}

enum Node {
    Null,
    String(String),
    Array(Vec<Node>),
}

fn to_json(node: &Node) -> String {
    match node {
        Node::Null => "null".to_string(),
        Node::String(s) => format!("\"{}\"", s),
        Node::Array(nodes) => format!("[{}]", nodes.iter().map(|n| to_json(n)).collect::<Vec<_>>().join(",").to_string()),
    }
}