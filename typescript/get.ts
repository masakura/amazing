const get = <T extends Record<string, unknown>, N extends keyof T>(obj: T, name: N): T[N] => {
    return obj[name];
};

const i = get({ a: 15, b: 'hello' }, 'a');
