const omit = <T extends Record<string, unknown>, N extends keyof T>(obj: T, name: N): Omit<T, N> => {
    const r: Record<string, unknown> = {};
    for (const key of Object.keys(obj)) {
        if (key === name) continue;
        r[key] = obj[key];
    }
    return r as Omit<T, N>;
}

const i = omit({ a: 1, b: 'hello' }, 'b');
console.log(i);
