# Java
Java でも Go のような書き方で変わるってのがある。

```Java
public class Consts {
    public static final int i = 20;
}
```

この `i` は定数。`Consts.i` を利用しているところは直接埋め込まれる。なので、`i` を変更しただけでは変更が伝わらない。

```Java
public class Consts {
    public static final int i = get(20);

    private static int get(int i) {
        return i;
    }
}
```

こうすると、変更不可能なメンバー変数になる。なので、`i` を変更したら変更が伝わる。